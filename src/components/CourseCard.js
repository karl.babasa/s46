import React, {useState} from 'react';
import {Card, Button} from 'react-bootstrap';
import PropTypes from 'prop-types'

export default function CourseCard({courseProp}) {

	//console.log(props)
	//console.log(typeof props)

	//Deconstruct the courseProp into their own variables.
	const {name, description, price} = courseProp;

	//Let's create our useState hooks to store its state
	//States are used to keep track of information related to individeual components
	/*Syntax:
		const [currentValue(getter), updatedValue(setter)] = useState(InitialGetterValue)
	
	*/

	/*Activity*/

	const [enrollCount, enrollSetCount] = useState(0);
	const [seatCount, seatSetCount] = useState(30);

	// /console.log(count);

	const enroll = () => {
		
	if (enrollCount >= 30) {
		return alert('no more seats left')
	} else {

		enrollSetCount(enrollCount + 1);
		seatSetCount(seatCount - 1)
		console.log("Enrollees: " + enrollCount)
	}

	

	}



	return(

		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>
				<Card.Text>Enrolees: {enrollCount}</Card.Text>
				<Card.Text>Seats: {seatCount}</Card.Text>
				<Button variant="primary" onClick={enroll}>Enroll</Button>
			</Card.Body>
		</Card>

		/*<Row className="py-3">
			<Col>
				<Card>
					<Card.Body>
						<Card.Title>
							<h4>Sample Course</h4>
						</Card.Title>
						<Card.Text>
							<h6>Description</h6>
							<p>This is a sample course offering</p>

							<h6>Price:</h6>
							<p>Php 40,000</p>
						</Card.Text>
						<Button variant="primary">Enroll</Button>
					</Card.Body>	
				</Card>
			</Col>
		</Row>*/
		)
}

//check if the CourseCard component is getting the correct prop types
//PropTypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from component to the next.

CourseCard.propTypes = {
	//shape() method it is used to check if a prop conforms to a specific 'shape'
	courseProp: PropTypes.shape({
		//Define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}