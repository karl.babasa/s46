import React from 'react';
import './App.css';
import AppNavBar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import {Container} from 'react-bootstrap';


function App() {
  return (
    <>
      <AppNavBar/>
      <Container>
        <Home/>
        <Courses/>
      </Container>
    </>
  );
}   

export default App;

//fo adding Javascript(Babel) Linting in sublime text
// ctrl shift P > tpe install > click the Package Control Install > Babel


